import React from 'react';
import axios from 'axios';
import Spinner from '../components/Spinner/Spinner';
import { DIT_DATA, SERVER_API_URL } from '../constants';

export default function asyncContainer(WrappedComponent) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        loaded: false
      };
      this.data = null;
    }

    componentDidMount() {
      if (localStorage.getItem(DIT_DATA)) {
        this.data = JSON.parse(localStorage.getItem(DIT_DATA));
        this.setState({ loaded: true });
      } else {
        axios.get(SERVER_API_URL).then(res => {
          this.data = res.data;
          localStorage.setItem(DIT_DATA, JSON.stringify(this.data));
          this.setState({ loaded: true });
        });
      }
    }

    render() {
      if (this.state.loaded) {
        return <WrappedComponent data={this.data} />;
      }
      return <Spinner />;
    }
  };
}
