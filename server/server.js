const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mainRouter = require('./routes/main');
const PORT = require('./constants').PORT;

function startServer() {
  const app = express();

  app.use(cors());
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  );
  app.use(mainRouter);
  app.listen(PORT, () => {
    console.log(`Server has started on ${PORT}.`);
  });
}
module.exports = startServer;
