const getDb = require('../db/utils').getDb;

async function getAllEmployees() {
  const db = await getDb();

  const [employees] = await db.get(
    `
    SELECT e.id, DATE_FORMAT(e.birthday, "%d.%m.%Y") AS birthday, e.firstname AS firstName, e.secondname AS secondName, e.email, e.position, e.vacationdate AS vacationDate, e.phone, e.department_id, d.title AS departmentTitle, GROUP_CONCAT(dc.ancestor_id ORDER BY dc.ancestor_id ASC) AS departments
    FROM employees e
    LEFT JOIN departments d ON e.department_id = d.id
    LEFT JOIN departments_closure dc ON e.department_id = dc.descendant_id GROUP BY e.id
  `
  );

  return employees;
}
exports.getAllEmployees = getAllEmployees;

async function getEmployee(id) {
  const db = await getDb();

  const [employee] = await db.get(
    `
    SELECT e.id, DATE_FORMAT(e.birthday, "%d.%m.%Y") AS birthday, e.firstname AS firstName, e.secondname AS secondName, e.email, e.position, e.vacationdate AS vacationDate, e.phone, e.department_id, d.title AS departmentTitle
    FROM employees e
    LEFT JOIN departments d ON e.department_id = d.id
    WHERE e.id = $1
  `,
    id
  );

  return employee;
}
exports.getEmployee = getEmployee;

async function getEmployeePassword(email) {
  const db = await getDb();

  const [[employee]] = await db.get(
    `
    SELECT e.id, e.password
    FROM employees e
    WHERE e.email = $1
  `,
    email
  );

  return employee;
}
exports.getEmployeePassword = getEmployeePassword;
